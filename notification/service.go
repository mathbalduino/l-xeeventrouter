package notification

import (
	"message_broker/notification/logic"
	"message_broker/publication"
)

// Notify will handle all the trouble to notify the subscribers
// and ensure that the operation was a success
func Notify(evt *publication.event) {
	// 1. Classify the event
	evtType := logic.classifyEvent(evt)

	// 2. Call the appropriate notification strategy
	failedOnes := logic.notifyStrategy[evtType](evt)

	// 3. Add the failed notifications to the pending list
	if len(failedOnes.Workers) != 0 || len(failedOnes.Clusters) != 0 {
		newPendingEvent(&pendingEvent{
			Evt:     evt,
			Targets: failedOnes,
		})
	}
}
