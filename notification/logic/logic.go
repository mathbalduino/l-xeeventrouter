package logic

type coreLogic struct {
	clusterStore
	channelStore
	eventStore
	net
	util
	log
}

func (c *coreLogic) Notify(h host, evt event) error {
	channel, err := c.channelStore.EventChannel(evt.Name())
	if err != nil {
		return c.util.Err(ChannelStoreCallErr, "Error calling function to retrieve [%s] event channel: %w", evt.Name(), err)
	}

	errOccurred := false
	for _, cluster := range channel {
		switch evt.Type() {
		case none:
		case get:
			e := c.notifyOnePerCluster(evt, cluster)
			if e != nil {
				errOccurred = true
				c.log.Error("Error occurred when notifying [%s] cluster: %v", cluster, e)
			}
		case post:
			e := c.notifyEntireCluster(evt, cluster)
			if e != nil {
				errOccurred = true
				c.log.Error("Error occurred when notifying [%s] cluster: %v", cluster, e)
			}
		default:
			errOccurred = true
			e := c.util.Err(UnrecognizedEvtType, "The [%s] event type is not recognizable", evt.Type())
			c.log.Error("Error occurred when notifying [%s] cluster: %v", cluster, e)
		}
	}
	if errOccurred {
		return c.util.Err(PublicationError, "An error occurred when notifying some cluster. See logs for details")
	}
	return nil
}

func (c *coreLogic) notifyOnePerCluster(evt event, cluster string) error {
	// 0. Create a channel to wait for the "DONE" event

	// 1. Iterate cluster
	closure := c.clusterStore.IterateClusterByLatency(cluster)
	for host := closure(); host != nil; host = closure() {
		// 2. Still have elements in the cluster

		// Ignore zombie ones
		if host.Zombie() {
			continue
		}

		// 3. Try to send the request
		e := c.net.Request(host.Address(), evt.Name(), evt.Data())

		// 3.1. Request error
		if e != nil {
			// 3.1.1. Kill POD
			killErr := c.clusterStore.Kill(host.Address())
			if killErr != nil {
				c.log.Error("Can't kill instance [%s:%s]: %v", host.Name(), host.Address(), killErr)
			}

			// 3.1.2. Continue the iteration
			continue
		}

		// 4. Request success. Wait for the "DONE" equivalent event

		// 4.1. "DONE" not received after X time slice

		// 4.2. "DONE" equivalent event received and processed by the target. Notify completed
		return nil
	}

	// No more elements in the cluster and the event didn't sent.
	// Put this event as a pending one to sent it in the future
	c.eventStore.ToRetry(evt)
	return c.util.Err(PublicationError, "Error publishing [%s] event to the [%s] cluster", evt.Name(), cluster)
}

func (c *coreLogic) notifyEntireCluster(evt event, cluster string) error {
	// 1. Iterate cluster
	closure := c.clusterStore.IterateClusterByLatency(cluster)
	for host := closure(); host != nil; host = closure() {
		// 2. Still have elements in the cluster

		// Ignore zombie ones
		if host.Zombie() {
			continue
		}

		// 3. Try to send the request
		e := c.net.Request(host.Address(), evt.Name(), evt.Data())

		// 3.1. Request error
		if e != nil {
			// 3.1.1. Kill POD if any error occurred
			killErr := c.clusterStore.Kill(host.Address())
			if killErr != nil {
				c.log.Error("Can't kill instance [%s:%s]: %v", host.Name(), host.Address(), killErr)
			}
		}
		// continue iteration
	}

	// No more elements in the cluster. Exit
	return nil
}
