package logic

// Entities

type host interface {
	Name() string
	Address() string
	Latency() uint64
	Zombie() bool
}

type event interface {
	Data() interface{}
	Name() string
	Type() string
}

// Modules

type eventStore interface {
	ToRetry(e event)
}

type clusterStore interface {
	IterateClusterByLatency(name string) func() host
	Kill(addr string) error
	AddPending(addr string, evt event)
}

type log interface {
	Info(m string, i ...interface{})
	Error(m string, i ...interface{})
	Warning(m string, i ...interface{})
}

type channelStore interface {
	EventChannel(evt string) ([]string, error)
}

type net interface {
	Request(addr string, evt string, data interface{}) error
}

type util interface {
	Err(code int, msg string, i ...interface{}) error
	IsNil(i interface{}) bool
}
