package publication

import (
	"message_broker/global"
	"sync"
)

var lastId uint64 = 0

// eventHistory contains the entire event history of the application
var eventHistory []*global.event

// mutex is the eventHistory queue mutex
var mutex = sync.Mutex{}

// saveEvent will just append the new event to the event history
func saveEvent(ev *global.event) {
	mutex.Lock()
	eventHistory = append(eventHistory, ev)
	mutex.Unlock()
}

// newEvent will create a new event and save it to the history.
func newEvent(name string, publisher string, data interface{}) *global.event {
	// 0. Increment counter
	lastId++

	// 1. Create a new event
	evt := &global.event{
		Id:			lastId,
		Name:      name,
		Publisher: publisher,
		Data:      data,
		Date:      0,
	}

	// 2. When a new event is created, it's immediately saved to the history
	saveEvent(evt)

	// 3. Return the new event
	return evt
}
