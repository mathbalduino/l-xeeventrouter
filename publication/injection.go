package publication

type Worker interface {
	Zombie() bool
	Address() string
	Name() string
}
var WorkerStore interface {
	Worker(address string) Worker
}

var NotifyService interface {
	Notify(eventId uint64)
}
