package logic

const (
	ReqContractSerializationErr = iota
	InvalidArgumentsErr
	UnreachableHostErr
	Non200StatusCodeErr
)
