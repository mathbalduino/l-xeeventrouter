package util

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
)

/*
Deserialize will create a struct from the given http request, which must
contain a JSON
*/
func Deserialize(r *http.Request, t interface{}) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(body, t)
	if err != nil {
		panic(err)
	}
}

// NewErrorf is just a helper to throw errors with the Sprintf function
// encapsulated inside it
func NewErrorf(msg string, v ...interface{}) error {
	return errors.New(fmt.Sprintf(msg, v...))
}

func IsNil(i interface{}) bool {
	return i == nil || (reflect.ValueOf(i).Kind() == reflect.Ptr && reflect.ValueOf(i).IsNil())
}

// ---

type err struct {
	e error
	code int
}

func (e *err) Unwrap() error { return e.e }
func (e *err) Error() string { return e.e.Error() }
func (e *err) Code() int { return e.code }
func (e *err) As(i interface{}) bool { return i.(int) == e.code }

func newError(code int, msg string, i ...interface{}) error {
	return &err{
		e:    fmt.Errorf(msg, i),
		code: code,
	}
}

// ---
