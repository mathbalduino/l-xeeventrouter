package util

import "reflect"

type Util struct {}

func (u *Util) Err(code int, msg string, i ...interface{}) error { return newError(code, msg, i) }
func (u *Util) IsNil(i interface{}) bool {
	return i == nil || (reflect.ValueOf(i).Kind() == reflect.Ptr && reflect.ValueOf(i).IsNil())
}
