package webserver

import (
	"message_broker/channels"
	"message_broker/publication"
	"message_broker/pods"
	"net/http"

	"github.com/gorilla/mux"
)

func initializeRoutes(router *mux.Router) {
	router.HandleFunc("/subscribe", asyncEndpoint(channels.SubscribeEndpoint))
	router.HandleFunc("/unsubscribe", asyncEndpoint(channels.UnsubscribeEndpoint))

	router.HandleFunc("/publish", asyncEndpoint(publication.PublishEndpoint))

	router.HandleFunc("/register", asyncEndpoint(pods.RegistrationEndpoint))
	router.HandleFunc("/unregister", asyncEndpoint(pods.UnregistrationEndpoint))

	router.HandleFunc("/health", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(200)
	})
}
