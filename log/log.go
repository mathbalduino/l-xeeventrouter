package log

import (
	"log"
	"os"
)

var (
	Info		*log.Logger = log.New(os.Stdout, "[ INFO  ] ", log.Ldate|log.Ltime)
	Warning		*log.Logger = log.New(os.Stdout, "[ WARN  ] ", log.Ldate|log.Ltime)
	Error		*log.Logger = log.New(os.Stderr, "[ ERROR ] ", log.Ldate|log.Ltime)
)

func LogInfo(format string, v ...interface{}) {
	Info.Printf(format + "\n", v...)
}
func LogWarn(format string, v ...interface{}) {
	Warning.Printf(format + "\n", v...)
}
func LogError(format string, v ...interface{}) {
	Error.Printf(format + "\n", v...)
}
