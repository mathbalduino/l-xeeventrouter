package logic

const (
	InvalidArgumentsErr = iota
	StoreFetchErr
	StoreSaveErr
	StoreDeleteErr
)
