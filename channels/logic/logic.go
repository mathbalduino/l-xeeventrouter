package logic

type coreLogic struct {
	store persistence
	log
	util
}

func (core *coreLogic) Subscribe(evt string, cluster string) error {
	if evt == "" || cluster == "" {
		return core.util.Err(InvalidArgumentsErr, "[%s] and [%s] aren't valid arguments in order to subscribe", evt, cluster)
	}

	evtSubscribers, e := core.store.Fetch(evt)
	if e != nil {
		return core.util.Err(StoreFetchErr, "Error fetching [%s] event data from the store: %w", evt, e)
	}
	for _, subName := range evtSubscribers {
		if subName == cluster {
			core.log.Warning("[%s] is already listening to the [%s] event channel", cluster, evt)
			return nil
		}
	}

	e = core.store.Save(evt, cluster)
	if e != nil {
		return core.util.Err(StoreSaveErr, "Error saving [%s] cluster subscription for [%s] event to the store: %w", cluster, evt, e)
	}

	core.log.Info("[%s] is now subscribing to the [%s] event channel", cluster, evt)
	return nil
}

func (core *coreLogic) Unsubscribe(evt string, cluster string) error {
	if evt == "" || cluster == "" {
		return core.util.Err(InvalidArgumentsErr, "[%s] and [%s] aren't valid arguments in order to unsubscribe", evt, cluster)
	}

	e := core.store.Delete(evt, cluster)
	if e != nil {
		return core.util.Err(StoreDeleteErr, "Error deleting the [%s] cluster subscription for [%s] event from the store: %w", evt, cluster, e)
	}

	core.log.Info("[%s] cluster successfully removed from the [%s] event channel", cluster, evt)
	return nil
}

func (core *coreLogic) EventChannel(evt string) ([]string, error) {
	if evt == "" {
		return nil, core.util.Err(InvalidArgumentsErr, "[%s] isn't a valid argument to retrieve ", evt)
	}

	slice, e := core.store.Fetch(evt)
	if e != nil {
		return nil, core.util.Err(StoreFetchErr, "Error fetching [%s] event data from the store: %w", evt, e)
	}
	return slice, nil
}
