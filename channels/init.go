package channels

import (
	"github.com/pkg/errors"
	"message_broker/channels/logic"
	"message_broker/channels/repository"
	"message_broker/util"
	"net/http"
)

type service interface {
	Subscribe(evt, cluster string) error
	Unsubscribe(evt, cluster string) error
	EventChannel(evt string) ([]string, error)
}

func DefaultService() (service, error) {
	internalLogic, e := logic.New(logic.Props{
		Store: repository.NewRepository(),
		Log:   nil,
		Util:  &util.Util{},
	})
	if e != nil {
		return nil, e
	}
	return internalLogic, nil
}

// ----

type Api interface {
	Subscribe(w http.ResponseWriter, r *http.Request)
	Unsubscribe(w http.ResponseWriter, r *http.Request)
}

func DefaultApi(s service, c clusterStore) (Api, error) {
	if s == nil || c == nil {
		return nil, errors.New("Error")
	}
	return &endpoints{s,c}, nil
}
