package channels

type clusterStore interface {
	ClusterExists(name string) bool
}
