package repository

type repo struct {
	*ram
	*db
}

func (r *repo) Save(evt, cluster string) error {
	dbErr := r.db.Save(evt, cluster)
	if dbErr != nil {
		return dbErr
	}
	// Never fails
	r.ram.Save(evt, cluster)
	return nil
}

func (r *repo) Delete(evt, cluster string) error {
	dbErr := r.ram.Delete(evt, cluster)
	if dbErr != nil {
		return dbErr
	}
	// Never fails
	r.ram.Delete(evt, cluster)
	return nil
}

func (r *repo) Fetch(evt string) ([]string, error) {
	return r.ram.Fetch(evt)
}
