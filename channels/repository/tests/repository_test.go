package tests

import (
	"message_broker/channels/repository"
	"testing"
)

func TestSave(t *testing.T) {
	repo := repository.NewRepository()
	if repo == nil {
		t.Fatal("The test was expected to get a valid repository")
	}

	chann, e := repo.Fetch("Test")
	if e != nil {
		t.Fatal("Unexpected error returned from Fetch method")
	}
	if len(chann) != 0 {
		t.Fatal("The repository should start empty")
	}

	e = repo.Save("Test", "test")
	if e != nil {
		t.Fatal("Unexpected error returned from Save method")
	}

	chann, e = repo.Fetch("Test")
	if e != nil {
		t.Fatal("Unexpected error returned from Fetch method")
	}
	if len(chann) != 1 {
		t.Fatal("The repository should have one element")
	}
}

func TestDelete(t *testing.T) {
	repo := repository.NewRepository()
	if repo == nil {
		t.Fatal("The test was expected to get a valid repository")
	}

	e := repo.Save("Test", "test")
	if e != nil {
		t.Fatal("Unexpected error returned from Save method")
	}

	e = repo.Delete("Test", "test")
	if e != nil {
		t.Fatal("Unexpected error returned from Delete method")
	}

	chann, e := repo.Fetch("Test")
	if e != nil {
		t.Fatal("Unexpected error returned from Fetch method")
	}
	if len(chann) != 0 {
		t.Fatal("The repository should have zero elements")
	}
}
