package repository

import "sync"

func NewRepository() *repo {
	return &repo{
		ram: &ram{make(map[string][]string), &sync.Mutex{}},
		db:  &db{},
	}
}
