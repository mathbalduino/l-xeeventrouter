package pods

import (
	"message_broker/log"
	"message_broker/util"
	"net/http"
)

// Api wraps the logic functions
type Api struct {
	logic LogicImplementation
}

// registrationContract is the expected JSON encoded POST argument that
// a Service must provide to register itself
type registrationContract struct {
	Name		string		`json:"name"`
	Addr		string		`json:"address"`
}

// RegistrationEndpoint will handle the incoming registration attempts from
// the services that are running.
func (a *Api) RegistrationEndpoint(w http.ResponseWriter, r *http.Request) {
	// 1. Deserialize the POST request data
	var reqArgs registrationContract
	util.Deserialize(r, &reqArgs)

	// 2. Register the worker. If it's already registered,
	// this is a noop
	pod, e := a.logic.Register(reqArgs.Name, reqArgs.Addr)
	if e != nil {
		panic(e)
	}

	// 3. Log the action
	log.LogInfo("Newly/Old [%s:%s] pod properly registered", pod.Name(), pod.Address())
}

// unregistrationContract is the expected JSON encoded POST argument that
// a Service must provide to unregister itself
type unregistrationContract struct {
	Addr		string		`json:"address"`
	Name		string		`json:"name"`
}

// UnregistrationEndpoint will handle all the incoming unregister
// requests from the active running WorkerInstances.
func (a *Api) UnregistrationEndpoint(w http.ResponseWriter, r *http.Request) {
	// 1. Deserialize the POST request data
	var reqArgs unregistrationContract
	util.Deserialize(r, &reqArgs)

	pod := a.logic.One(reqArgs.Addr)
	if util.IsNil(pod) {
		log.LogWarn("You're trying to unregister a nonexistent pod. Address: [%s]", reqArgs.Addr)
		return
	}

	// 2. Try to unregister the given worker. Errors can happen
	e := a.logic.Unregister(pod)
	if e != nil {
		panic(e)
	}

	// 3. If success then log it
	log.LogInfo("The worker with the [%s] address has been unregistered", reqArgs.Addr)
}
