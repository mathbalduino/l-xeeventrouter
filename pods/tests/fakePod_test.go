package tests

import (
	"github.com/stretchr/testify/mock"
	"message_broker/global"
	"message_broker/util"
)

type mockMutex struct {
	mock.Mock
}

func (m *mockMutex) Lock()   { m.Called() }
func (m *mockMutex) Unlock() { m.Called() }

// ---

type fakePod struct {
	name    string
	address string
	latency uint64
	zombie  bool
	mutex   *mockMutex
}

func (p *fakePod) Name() string    { return p.name }
func (p *fakePod) Address() string { return p.address }
func (p *fakePod) Latency() uint64 { return p.latency }
func (p *fakePod) Zombie() bool    { return p.zombie }

func newPod(name, address string) *fakePod {
	return &fakePod{
		name:    name,
		address: address,
		latency: 0,
		zombie:  false,
		mutex:   &mockMutex{},
	}
}
func isPodEq(p *fakePod, w global.Worker) bool {
	if util.IsNil(w) && util.IsNil(p) {
		return true
	}
	if !util.IsNil(w) && !util.IsNil(p) {
		return w.Name() == p.name && w.Address() == p.address && w.Latency() == p.latency && w.Zombie() == p.zombie
	}
	return false
}

