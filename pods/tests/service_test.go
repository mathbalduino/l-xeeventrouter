package tests

import (
	"message_broker/pods"
	"message_broker/pods/logic"
	test2 "message_broker/util/testUtil"
	"testing"
)

func TestRegister(t *testing.T) {
	podsService, _ := pods.DefaultService()

	type test struct {
		inputName string
		inputAddr string
		expectedPod	*fakePod
		expectedErr error
	}
	tests := []test{
		{"", "", nil, logic.InvalidNameAddrSaveError},
		{"ValidName", "", nil, logic.InvalidNameAddrSaveError},
		{"ValidName", "invalidURL", nil, logic.InvalidNameAddrSaveError},
		{"", "http://valid.url", nil, logic.InvalidNameAddrSaveError},
		{"", "invalidURL", nil, logic.InvalidNameAddrSaveError},
		{"ValidName", "http://valid.url", newPod("ValidName", "http://valid.url"), nil},
		{"ValidName", "http://some.url/any/thing", newPod("ValidName", "http://some.url/any/thing"), nil},
		{"ValidName", "http://some.url/any/thing?var=1", newPod("ValidName", "http://some.url/any/thing?var=1"), nil},
		{"ValidName", "http://some.url/any/thing#slug", newPod("ValidName", "http://some.url/any/thing#slug"), nil},
		{"ServiceA", "http://unique.url", newPod("ServiceA", "http://unique.url"), nil},
		{"ServiceB", "http://unique.url", newPod("ServiceB", "http://unique.url"), nil},
	}

	for _, test := range tests {
		pod, e := podsService.Register(test.inputName, test.inputAddr)
		if e != test.expectedErr {
			t.Fatal(test2.NotEqual("ERROR", test.expectedErr, e))
		}
		if !isPodEq(test.expectedPod, pod) {
			t.Fatal(test2.NotEqual("POD", test.expectedPod, pod))
		}
	}
}
