package logic

type Logic interface {
	Register(name string, address string) (Pod, error)
	Unregister(pod Pod) error
	Kill(pod Pod) error
	Wakeup(pod Pod) error
	SetLatency(pod Pod, latency uint64) (uint64, error)
	IsRegistered(address string) bool
	One(address string) Pod
	Iterate(cmp func(a, b Pod) bool) func() Pod
	IterateByLatency() func() Pod
	IterateCluster(clusterName string, cmp func(a, b Pod) bool) func() Pod
	IterateClusterByLatency(clusterName string) func() Pod
}

type privateLogic struct{}

// Mutations
func (l *privateLogic) Register(name string, address string) (Pod, error) {
	return registerPod(name, address)
}
func (l *privateLogic) Unregister(w Pod) error {
	return unregisterPod(w)
}
func (l *privateLogic) Kill(w Pod) error {
	return disablePod(w)
}
func (l *privateLogic) Wakeup(w Pod) error {
	return enablePod(w)
}
func (l *privateLogic) SetLatency(w Pod, latency uint64) (uint64, error) {
	return changePodLatency(w, latency)
}

// Queries
func (l *privateLogic) IsRegistered(address string) bool {
	return podExists(address)
}
func (l *privateLogic) One(address string) Pod {
	return fetchOne(address)
}
func (l *privateLogic) Iterate(cmp func(a, b Pod) bool) func() Pod {
	return fetchAll(cmp)
}
func (l *privateLogic) IterateByLatency() func() Pod {
	return fetchAll(func(a, b Pod) bool {
		return a.Latency() < b.Latency()
	})
}
func (l *privateLogic) IterateCluster(clusterName string, cmp func(a, b Pod) bool) func() Pod {
	return fetchCluster(clusterName, cmp)
}
func (l *privateLogic) IterateClusterByLatency(clusterName string) func() Pod {
	return fetchCluster(clusterName, func(a, b Pod) bool {
		return a.Latency() < b.Latency()
	})
}
