package logic

import (
	"github.com/pkg/errors"
	"message_broker/log"
	"message_broker/util"
	"net/url"
)

// isUrl is used to validate the given string,
// checking to see if it is a valid URL
func isUrl(str string) bool {
	u, err := url.Parse(str)
	return err == nil && u.Scheme != "" && u.Host != ""
}

// registerPod will create a new Worker and try to save it to the
// ram and db, in this order.
//
// If there's already any Worker with the given address, return it
// immediately
func registerPod(name string, address string) (Pod, error) {
	if name == "" || !isUrl(address) {
		return nil, InvalidNameAddrSaveError
	}

	// 1. Is already registered?
	pod, _ := ram.Fetch(address)
	if pod != nil {
		return pod, nil
	}

	// 2. Try to add it to the ram. RAM dont throw errors
	_, _ = ram.Save(name, address)

	// 3. Try to add it to the db
	pod, e := db.Save(name, address)
	if e != nil {
		// TODO: What happens if the db isn't working? The Pod is already saved to the ram
		log.LogError("%+v", errors.Wrapf(e, "Error when saving a new [%s:%s] Worker to the database", name, address))
		return nil, DbConnectionError
	}

	return pod, nil
}

// unregisterPod will try to remove the given pod
// from the ram and db, is this order
func unregisterPod(w Pod) error {
	if w == nil {
		return NilPodError
	}
	// 1. Remove from the ram first
	_ = ram.Delete(w.Address())

	// 2. Remove from the db
	e := db.Delete(w.Address())
	if e != nil {
		// TODO: What happens if the db fails? The Worker was removed from the ram
		log.LogError("%+v", errors.Wrapf(e, "Error when removing the [%s:%s] Worker from the db", w.Name(), w.Address()))
		return DbConnectionError
	}

	log.LogInfo("[%s:%s] Pod successfully removed", w.Name(), w.Address())
	return nil
}

// disablePod will disable some Worker, preventing it
// from receiving/sending events over the message broker
func disablePod(w Pod) error {
	if util.IsNil(w) {
		return NilPodError
	}
	w.Mutex().Lock()
	w.SetZombie(true)
	w.Mutex().Unlock()
	return nil
}

// enablePod will enable some Worker to receive/send
// events over the message broker
func enablePod(w Pod) error {
	if util.IsNil(w) {
		return NilPodError
	}
	w.Mutex().Lock()
	w.SetZombie(false)
	w.Mutex().Unlock()
	return nil
}

// changePodLatency will change the response time of some Worker
// and save it to the db
func changePodLatency(w Pod, latency uint64) (uint64, error) {
	if util.IsNil(w) {
		return 0, NilPodError
	}
	w.Mutex().Lock()
	old := w.Latency()
	w.SetLatency(latency)
	w.Mutex().Unlock()
	return old, nil
}
