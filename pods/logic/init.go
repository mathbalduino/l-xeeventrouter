package logic

// New will take the implementations of DB and RAM and
// inject it into the appropriate vars
func New(injectedDB Repository, injectedRAM Repository) (Logic, error) {
	if injectedRAM == nil || injectedDB == nil {
		return nil, NilRepositoryNewLogicError
	}
	db = injectedDB
	ram = injectedRAM
	return &privateLogic{}, nil
}
