package tests

import (
	"github.com/stretchr/testify/mock"
	"message_broker/pods/logic"
	"sync"
)

type mockRepository struct {
	mock.Mock
}

func (m *mockRepository) Save(name string, address string) (logic.Pod, error) {
	args := m.Called(name, address)
	a := args.Get(0)
	if a == nil {
		return nil, args.Error(1)
	}
	return a.(logic.Pod), args.Error(1)
}
func (m *mockRepository) Delete(address string) error {
	args := m.Called(address)
	return args.Error(0)
}
func (m *mockRepository) Fetch(address string) (logic.Pod, error) {
	args := m.Called(address)
	ret := args.Get(0)
	if ret == nil {
		return nil, args.Error(1)
	}
	return ret.(logic.Pod), args.Error(1)
}
func (m *mockRepository) FetchAll() ([]logic.Pod, error) {
	args := m.Called()
	ret := args.Get(0)
	if ret == nil {
		return nil, args.Error(1)
	}
	return ret.([]logic.Pod), args.Error(1)
}
func (m *mockRepository) Count() (uint64, error) {
	args := m.Called()
	return args.Get(0).(uint64), args.Error(1)
}

// ---

type mockMutex struct {
	mock.Mock
}

func (m *mockMutex) Lock()   { m.Called() }
func (m *mockMutex) Unlock() { m.Called() }

// ---

type fakePod struct {
	name    string
	address string
	latency uint64
	zombie  bool
	mutex   *mockMutex
}

func (p *fakePod) Name() string        { return p.name }
func (p *fakePod) Address() string     { return p.address }
func (p *fakePod) Latency() uint64     { return p.latency }
func (p *fakePod) Zombie() bool        { return p.zombie }
func (p *fakePod) Mutex() sync.Locker  { return p.mutex }
func (p *fakePod) SetZombie(z bool)    { p.zombie = z }
func (p *fakePod) SetLatency(l uint64) { p.latency = l }
