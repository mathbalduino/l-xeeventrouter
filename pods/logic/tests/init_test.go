package tests

import (
	"message_broker/pods/logic"
	test2 "message_broker/util/testUtil"
	"testing"
)

func TestNew(t *testing.T) {
	type test struct {
		inputDB     logic.Repository
		inputRAM    logic.Repository
		expectLogic logic.Logic
		expectErr   error
	}
	tests := []test{
		{nil, nil, nil, logic.NilRepositoryNewLogicError},
		{nil, &mockRepository{}, nil, logic.NilRepositoryNewLogicError},
		{&mockRepository{}, nil, nil, logic.NilRepositoryNewLogicError},
		{&mockRepository{}, &mockRepository{}, nil, nil},
	}

	for _, test := range tests {
		l, e := logic.New(test.inputDB, test.inputRAM)
		if e != nil && e != test.expectErr {
			t.Fatalf(test2.NotEqual("ERROR", test.expectErr, e))
		}
		if e == nil && l == nil {
			t.Fatalf(test2.NotEqual("LOGIC", "non-nil", nil))
		}
	}
}
