package logic

// Repository represents the functions that
// must be implemented by the Repository layer
// in order to the logic work correctly
type Repository interface {
	Save(name string, address string) (Pod, error)
	Delete(address string) error
	Fetch(address string) (Pod, error)
	FetchAll() ([]Pod, error)
	Count() (uint64, error)
}

// db must implement the operations
// involved with database persistency
var db Repository

// ram must implement the operations
// involved with the runtime store of
// Pod
var ram Repository
