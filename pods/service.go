package pods

import (
	"message_broker/pods/logic"
	"message_broker/global"
)

// LogicImplementation represents the operations that any used logic
// must implement in order to satisfy this service
type LogicImplementation interface {
	logic.Logic
}

// Service wraps the logic functions
type Service struct {
	logic LogicImplementation
}

func (s *Service) Register(name string, address string) (global.Worker, error) {
	return s.logic.Register(name, address)
}
func (s *Service) Unregister(w global.Worker) error {
	return s.logic.Unregister(w.(logic.Pod))
}
func (s *Service) Kill(w global.Worker) error {
	return s.logic.Kill(w.(logic.Pod))
}
func (s *Service) Wakeup(w global.Worker) error {
	return s.logic.Wakeup(w.(logic.Pod))
}
func (s *Service) SetLatency(w global.Worker, latency uint64) (uint64, error) {
	return s.logic.SetLatency(w.(logic.Pod), latency)
}
func (s *Service) IsRegistered(address string) bool {
	return s.logic.IsRegistered(address)
}
func (s *Service) One(address string) global.Worker {
	return s.logic.One(address)
}
func (s *Service) Iterate(cmp func(a, b global.Worker) bool) func() global.Worker {
	var closure func() logic.Pod
	if cmp != nil {
		closure = s.logic.Iterate(func(a, b logic.Pod) bool {
			return cmp(a, b)
		})
	} else {
		closure = s.logic.Iterate(nil)
	}
	return func() global.Worker {
		return closure()
	}
}
func (s *Service) IterateByLatency() func() global.Worker {
	closure := s.logic.IterateByLatency()
	return func() global.Worker {
		return closure()
	}
}
func (s *Service) IterateCluster(clusterName string, cmp func(a, b global.Worker) bool) func() global.Worker {
	var closure func() logic.Pod
	if cmp != nil {
		closure = s.logic.IterateCluster(clusterName, func(a, b logic.Pod) bool {
			return cmp(a, b)
		})
	} else {
		closure = s.logic.IterateCluster(clusterName, nil)
	}
	return func() global.Worker {
		return closure()
	}
}
func (s *Service) IterateClusterByLatency(clusterName string) func() global.Worker {
	closure := s.logic.IterateClusterByLatency(clusterName)
	return func() global.Worker {
		return closure()
	}
}
