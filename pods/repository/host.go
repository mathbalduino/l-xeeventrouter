package repository

import (
	"sync"
)

// host holds the information about the services that are
// communicating to the message broker
type host struct {
	name    string      // Worker human readable Name
	address string      // Net Address to send/receive requests/responses
	latency uint64      // Average response time Latency (in ms)
	zombie  bool        // Zombie is used to return the request immediately due to high Latency
	mutex   *sync.Mutex // Used to control the concurrency on changes
}

// External
func (w *host) Name() string {
	return w.name
}
func (w *host) Address() string {
	return w.address
}
func (w *host) Latency() uint64 {
	return w.latency
}
func (w *host) Zombie() bool {
	return w.zombie
}

// Internal
func (w *host) Mutex() sync.Locker {
	return w.mutex
}
func (w *host) SetZombie(z bool){
	w.zombie = z
}
func (w *host) SetLatency(l uint64){
	w.latency = l
}
func newHost(name string, address string) *host {
	return &host{
		name:    name,
		address: address,
		latency: 0,
		zombie:  false,
		mutex:   &sync.Mutex{},
	}
}
