package tests

import (
	"message_broker/pods/logic"
	"message_broker/util"
)

func isPodEqual(a, b logic.Pod) bool {
	if util.IsNil(a) && util.IsNil(b) {
		return true
	}
	if util.IsNil(a) {
		return false
	}
	if util.IsNil(b) {
		return false
	}
	if a.Address() == b.Address() && a.Name() == b.Name() && a.Latency() == b.Latency() {
		return true
	}
	return false
}
