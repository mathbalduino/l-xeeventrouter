package tests

import (
	"math/rand"
	"message_broker/pods/logic"
	"testing"
	"time"
)

type factory struct {
	repo logic.Repository
	t    *testing.T
}

func (f *factory) fakeSet() (pods []logic.Pod) {
	clusterQt := (rand.Int() % 10) + 10
	podQt := (rand.Int() % 10) + 10
	for i := 0; i < clusterQt; i++ {
		name := string((rand.Int() % 26) + 65)
		for j := 0; j < podQt; j++ {
			addr := string((rand.Int() % 26) + 65)
			pods = append(pods, f.withNameAddr(name + "_Service", addr + "_Address"))
		}
	}
	return pods
}
func (f *factory) random() logic.Pod {
	name := string((rand.Int() % 26) + 65)
	addr := string((rand.Int() % 26) + 65)
	pod, e := f.repo.Save(name+"_Service", addr+"_Address")
	if e != nil {
		f.t.Fatal("Error when inserting in RAM repository")
		return nil
	}
	return pod
}
func (f *factory) withNameAddr(name, addr string) logic.Pod {
	pod, e := f.repo.Save(name, addr)
	if e != nil {
		f.t.Fatal("Error when inserting in RAM repository")
		return nil
	}
	return pod
}
func (f *factory) withName(name string) logic.Pod {
	addr := string((rand.Int() % 26) + 65)
	pod, e := f.repo.Save(name, addr+"_Address")
	if e != nil {
		f.t.Fatal("Error when inserting in RAM repository")
		return nil
	}
	return pod
}
func (f *factory) withAddr(addr string) logic.Pod {
	name := string((rand.Int() % 26) + 65)
	pod, e := f.repo.Save(name+"_Service", addr)
	if e != nil {
		f.t.Fatal("Error when inserting in RAM repository")
		return nil
	}
	return pod
}

func newFactory(ram logic.Repository, t *testing.T) *factory {
	rand.Seed(time.Now().UnixNano())
	return &factory{ram, t}
}
