package tests

import (
	"message_broker/pods/logic"
	"message_broker/pods/repository"
	"message_broker/util"
	test2 "message_broker/util/testUtil"
	"testing"
)

func TestSave(t *testing.T) {
	ram := repository.NewRAM()

	type test struct {
		inputName string
		inputAddr string
	}
	tests := []test{
		{"", ""},
		{"ServiceA", ""},
		{"", "addressA"},
		{"ServiceA", "addressA"},
	}

	for _, test := range tests {
		pod, e := ram.Save(test.inputName, test.inputAddr)
		if e != nil {
			t.Fatal(test2.NotEqual("ERROR", "nil", e))
		}
		if util.IsNil(pod) {
			t.Fatal(test2.NotEqual("POD", "not nil", "nil"))
		}
		if pod.Name() != test.inputName {
			t.Fatal(test2.NotEqual("NAME", test.inputName, pod.Name()))
		}
		if pod.Address() != test.inputAddr {
			t.Fatal(test2.NotEqual("ADDRESS", test.inputAddr, pod.Address()))
		}
	}
	count, e := ram.Count()
	if e != nil {
		t.Fatal(test2.NotEqual("ERROR", "nil", e))
	}
	if count != uint64(len(tests)) {
		t.Fatalf(test2.NotEqual("COUNT", len(tests), count))
	}
}

func TestDelete(t *testing.T) {
	ram := repository.NewRAM()
	factory := newFactory(ram, t)

	insertedPod := factory.random()

	type test struct {
		inputAddr string
	}
	tests := []test{
		{""},
		{"NonregisteredAddress"},
		{insertedPod.Address()},
	}

	for _, test := range tests {
		e := ram.Delete(test.inputAddr)
		if e != nil {
			t.Fatal(test2.NotEqual("ERROR", "nil", e))
		}
	}
	removed, e := ram.Fetch(insertedPod.Address())
	if e != nil {
		t.Fatal(test2.NotEqual("ERROR", "nil", e))
	}
	if !util.IsNil(removed) {
		t.Fatal(test2.NotEqual("POD", "nil", removed))
	}
}

func TestFetch(t *testing.T) {
	ram := repository.NewRAM()
	factory := newFactory(ram, t)

	insertedPod := factory.random()
	type test struct {
		inputAddr string
		expectPod logic.Pod
	}
	tests := []test{
		{"", nil},
		{"NonregisteredAddres", nil},
		{insertedPod.Address(), insertedPod},
	}

	for _, test := range tests {
		pod, e := ram.Fetch(test.inputAddr)
		if e != nil {
			t.Fatal(test2.NotEqual("ERROR", "nil", e))
		}
		if !isPodEqual(pod, test.expectPod) {
			t.Fatal(test2.NotEqual("POD", test.expectPod, pod))
		}
	}

}

func TestFetchAll(t *testing.T) {
	ram := repository.NewRAM()
	factory := newFactory(ram, t)

	expec := factory.fakeSet()
	recPods, e := ram.FetchAll()
	if e != nil {
		t.Fatal(test2.NotEqual("ERROR", "nil", e))
	}
	for i, pod := range recPods {
		if !isPodEqual(pod, expec[i]) {
			t.Fatal(test2.NotEqual("POD", expec[i], pod))
		}
	}
}

func TestCount(t *testing.T) {
	ram := repository.NewRAM()
	factory := newFactory(ram, t)

	count, e := ram.Count()
	if e != nil {
		t.Fatal(test2.NotEqual("ERROR", "nil", e))
	}
	if count != 0 {
		t.Fatal(test2.NotEqual("COUNT", 0, count))
	}

	factory.random()

	count, e = ram.Count()
	if count != 1 {
		t.Fatal(test2.NotEqual("COUNT", 1, count))
	}
}
