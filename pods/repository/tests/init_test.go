package tests

import (
	"message_broker/pods/repository"
	"message_broker/util/testUtil"
	"testing"
)

func TestNewRam(t *testing.T) {
	ram := repository.NewRAM()

	count, e := ram.Count()
	if e != nil {
		t.Fatal(testUtil.NotEqual("ERROR", "nil", e))
	}
	if count != 0 {
		t.Fatal(testUtil.NotEqual("COUNT", 0, count))
	}

	ram.Save("Any", "Any")
	ram.Save("Any2", "Any2")

	ram = repository.NewRAM()
	count, e = ram.Count()
	if e != nil {
		t.Fatal(testUtil.NotEqual("ERROR", "nil", e))
	}
	if count != 0 {
		t.Fatal(testUtil.NotEqual("COUNT", 0, count))
	}
}
