package repository

import "github.com/pkg/errors"

var WITHOUT_NAME_ERROR = errors.New("You must provide a NAME for this host")
var WITHOUT_ADDRESS_ERROR = errors.New("You must provide an ADDRESS for this host")
var WITHOUT_NAME_ADDR_ERROR = errors.New("You must provide an ADDRESS and a NAME for this host")
